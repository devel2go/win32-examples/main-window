/* main.go */

package main

import (
	
	"fmt"
	"os"
	"path/filepath"
	"gitlab.com/devel2go/win"
	//"gitlab.com/devel2go/go-gui/control"
	//h "gitlab.com/devel2go/go-gui/global"

)

var button1 *win.GoButtonControl
var button2 *win.GoButtonControl
var desktop *win.GoDeskTopWindow
var label1 *win.GoLabelControl
var mainwindow *win.GoWindowView
var secondwindow *win.GoWindowView


func main() {
	appName := filepath.Base(os.Args[0])
	appName = appName[:len(appName) - 4]
	fmt.Println("Application Name =", appName)
	app := win.GoApplication(appName)

	desktop = app.GoDeskTop()

	mainwindow = win.GoWindow(nil)
	mainwindow.SetTitle("New Main Window")
	font, _ := win.GoFont(win.GoFontDesc{Name: "Tahoma", Height: -11})
	mainwindow.SetFont(font)
	mainwindow.Show()

	fmt.Println("Creating GoButton........button1")
	button1 = win.GoButton(mainwindow)
	button1.SetOnClick(button1_click)
	button1.Show()
	//button1.SetX(40)

	secondwindow = win.GoWindow(mainwindow)
	secondwindow.SetTitle("New Second Window")
	secondwindow.Show()
	//fmt.Println("Creating MainWindow........")
	label1 = win.GoLabelEx(secondwindow, "Hello World")
	label1.Show()
	
	button2 = win.GoButton(secondwindow)
	button2.SetOnClick(button2_click)
	button2.SetPosition(10, 60)
	button2.Show()

	ret := app.Run()
	fmt.Println("Window Closed return =", ret)
}

func button1_click() {
	label1.SetText("Button1 GoWorld!")
}

func button2_click() {
	label1.SetText("Button2 GoWorld!")
}